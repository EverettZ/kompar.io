/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { StrongOrWeakComponent } from './strong-or-weak.component';

describe('StrongOrWeakComponent', () => {
  let component: StrongOrWeakComponent;
  let fixture: ComponentFixture<StrongOrWeakComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StrongOrWeakComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StrongOrWeakComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
