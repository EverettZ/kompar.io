export class ModelView {

    items = [];
    href = '';
    length = 0;

    constructor(mv: any) {

        this.items = mv.items;
        this.href = mv.href;
        this.length = mv.length;

    }

}

export class Character {

    // Data
    name = '';
    description = '';
    votes: KomparChar[] = [];

    // Toggles
    isSelected = false;

    // Styling
    profilePath = '';
    backgroundPath = '';

    constructor(mv: any) {

        this.name = mv.name || '';
        this.description = mv.description || '';

        mv.votes.forEach(element => {
            this.votes.push(new KomparChar(element));
        });

        this.isSelected = false;
        this.profilePath += this.name;
        this.backgroundPath += this.name;

    }

}

export class KomparChar {
    
    // Data    
    name = '';
    Strength = 0;

    // Styling

    constructor(mv: any) {

        this.name = mv.name;
        this.Strength = mv.strength;

    }

}