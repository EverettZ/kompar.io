import { KomparioPage } from './app.po';

describe('kompario App', function() {
  let page: KomparioPage;

  beforeEach(() => {
    page = new KomparioPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
